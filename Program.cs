﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_2_Getion_Magasin
{
    class Program
    {
        static void Main(string[] args)
        {
            // Création d'une liste de commande
            List<Commande> listCommande = new();

            // Création d'une liste de client
            List<Client> listClient = new();


            // Création des produits
            Produit prod1 = new Produit("Nutrition", "Poulet", 2500);
            Produit prod2 = new Produit("Nutrition", "Lapin", 3500);

            // Création des clients
            Client client1 = new Client("Particulier", "Mamadou", "Axel", "La Cité");
            Client client2 = new Client("Entreprise", "Dialo", "Franc", "Canada");

            // Création des commandes
            Commande cmd1 = new Commande(client1.NumeroCarteMembre, "15 mai", 2000, "Rapide");
            Commande cmd2 = new Commande(client2.NumeroCarteMembre, "19 Juin 2020", 5000, "Rapide");

            // Ajoute des commandes à la liste de commande
            listCommande.Add(cmd1);
            listCommande.Add(cmd2);
            
            // Ajoute des clients à la liste des clients
            listClient.Add(client1);
            listClient.Add(client2);

            // Ajoute de liste de commande et la liste Client au magasin
            Magasin magasinCentral = new Magasin(listCommande, listClient);


            //Ajoute des produits a la 1er commande
            cmd1.AjoutProduit(prod1);
            cmd1.AjoutProduit(prod2);

            //Affiche la commande du premier client avec ses information
            cmd1.AfficherCommande(cmd1, client1);

            //Ajoute des produits a la 2e commande
            cmd2.AjoutProduit(prod2);

            //Affiche la commande de deuxieme client avec ses information
            cmd2.AfficherCommande(cmd2, client2);

            //Enregistre dans un fichier la commande 1
            cmd1.ImprimerCommande();

            //Enregistre dans un fichier txt toutes les commandes avec le nom des clients 
            magasinCentral.ImprimerToutesCommandes();

        }
    }

}
