using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_2_Getion_Magasin
{
    #region Attributs
    internal class Commande
    {

          /// <summary>
        /// Les attributs
        /// </summary>
        public List<Produit> produitList = new();

        private static int prochainNumeroCommande = 1;
        public int NumeroCommande { get; set; }
        public int NumeroClient { get; set; }
        public string DateCommande { get; set; }
        public int MontantCommande { get; set; }
        public string TypeLivraison { get; set; }

        #endregion

        #region Constructeur
        /// <summary>
        /// Constructeur de la classe
        /// </summary>
        /// <param name="numeroClient"></param>
        /// <param name="dateCommande"></param>
        /// <param name="typeLivraison"></param>
        /// <param name="montantCommande"></param>
        public Commande(int numeroClient, string dateCommande, int montantCommande, string typeLivraison)
            
        {
            NumeroClient = numeroClient;
            DateCommande = dateCommande;
            MontantCommande = montantCommande;
            TypeLivraison = typeLivraison;


            NumeroCommande = prochainNumeroCommande;
            prochainNumeroCommande++;
        }
        #endregion

        #region  Ajouter Produit

        /// <summary>
        /// Ajouter les produits
        /// </summary>
        /// <param name="prod"></param>
        public void AjoutProduit(Produit prod)
        {
            produitList.Add(prod);
        }
        #endregion
       
        #region Rechercher produit

        /// <summary>
        /// Rechercher un produit
        /// </summary>
        public void RechercheProduit()
        {
            List<Produit> produit = TrouverLeProduit();

            if (produit.Count == 0)
            {
                Console.WriteLine("Ce produit n'a pu être retrouvé. Appuyez sur n'importe quelle touche pour continuer");
                Console.ReadKey();
                return;
            }

            Console.WriteLine("Voici les produits présentes dans votre liste correspondant à cette recherche :\n");
            foreach (var pro in produit)
            {
                AfficherProduits(pro);
            }
            Console.WriteLine("\nAppuyez sur n'importe quelle touche pour continuer.");
            Console.ReadKey();
        }

        public List<Produit> TrouverLeProduit()
        {
            Console.WriteLine("Saisissez le nom du produit que vous souhaitez rechercher.");
            string nom = Console.ReadLine();
            return produitList.Where(x => x.Nom.ToLower() == nom.ToLower()).ToList();
        }
        #endregion



        #region Afficher Produit

        /// <summary>
        /// Afficher produit
        /// </summary>
        /// <param name="prod"></param>
        public void AfficherProduits(Produit prod)
        {
            Console.WriteLine("Nom: " + prod.Nom);
            Console.WriteLine("Code: " + prod.CodeBar);
            Console.WriteLine("Prix: " + prod.Prix);
            Console.WriteLine("Categorie: " + prod.Categorie);
           
        }
        #endregion

        #region Afficher une commande
        /// <summary>
        /// Afficher une commande
        /// </summary>
        /// <param name="commande"></param>
        /// <param name="client"></param>
        public void AfficherCommande(Commande commande, Client client)
        {
            Console.WriteLine("Nom: " + client.Nom);
            Console.WriteLine("Prenom: " + client.Prenom);
            Console.WriteLine("Code: " + client.NumeroCarteMembre);
            Console.WriteLine("Type de client: " + client.TypeClient);
            Console.WriteLine("Adresse: " + client.Adresse);
            Console.WriteLine("Numéro de Client: " + commande.NumeroClient);
            Console.WriteLine("Date de livraison: " + commande.DateCommande);
            Console.WriteLine("Type de livraison: " + commande.TypeLivraison);
            Console.WriteLine();
            Console.WriteLine($"-----------Produits commandés par {client.Nom} -------------");
            Console.WriteLine();


            Console.Write("Nom\t\tCode\t\tPrix\t\tCategorie ");
            double montant = 0;
            foreach (Produit produit in produitList)
            {
          
                produit.AfficherProduit();
                montant+= produit.Prix;
            }
            Console.WriteLine("\n\nTotal = " + montant+"$\n\n");
            

        }
        #endregion

        #region Modifier Produit

        /// <summary>
        /// Modifier un produit
        /// </summary>
        public void ModifierProduit()
        {
            List<Produit> prod = TrouverLeProduit();
            if (prod.Count == 0)
            {
                Console.WriteLine("Ce produit n'a pu être retrouvé. Appuyez sur n'importe quelle touche pour continuer");
                Console.ReadKey();
                return;
            }
            if (prod.Count == 1)
            {
                return;
            }

        }
        #endregion

        #region Supprimer produit
        /// <summary>
        /// Supprimer un produit
        /// </summary>
        public void SupprimerProduit()
        {
            List<Produit> produit = TrouverLeProduit();


            //Condition verifiant si le produit existe
            if (produit.Count == 0)
            {
                Console.WriteLine("Ce produit n'a pu être retrouvé. Appuyez sur n'importe quelle touche pour continuer");
                Console.ReadKey();
                return;
            }

            if (produit.Count == 1)
            {
                RetirerProduitListe(produit.Single());
                return;
            }
            
            //Supprimer un produit a l'aide de son code produit

            Console.WriteLine("Entrez le Code du produit que vous souhaitez supprimer");
            for (int i = 0; i < produit.Count; i++)
            {
                Console.WriteLine(i);
                AfficherProduits(produit.ElementAt(i));
            }
            int retirerProduitCode = Convert.ToInt32(Console.ReadLine());

            //Condition verifiant si le produit se trouve dans  la liste
            if (retirerProduitCode > produit.Count - 1 || retirerProduitCode < 0)
            {
                Console.WriteLine("Ce code est invalide. Appuyez sur n'importe quelle touche pour continuer.");
                Console.ReadKey();
                return;
            }
            RetirerProduitListe(produit.ElementAt(retirerProduitCode));
        }
        /// <summary>
        /// Retirer un produit de la liste des produits
        /// </summary>
        /// <param name="prod"></param>
        /// 

        public void RetirerProduitListe(Produit prod)
        {
            Console.Clear();
            Console.WriteLine("Voulez-vous vraiment supprimer ce produit de votre liste ? (O/N)");
            AfficherProduits(prod);

            //Condition pour retirer le produit de la liste si le choix de l'utilisateur est O

            if (Console.ReadKey().Key == ConsoleKey.O)
            {
                produitList.Remove(prod);
                Console.Clear();
                Console.WriteLine("Produit supprimé. Appuyez sur n'importe quelle touche pour continuer.");
                Console.ReadKey();
            }
        }

        /// <summary>
        /// Permet d'enregistrer les fichiers
        /// </summary>
        public void ImprimerCommande()
        {
            List<string> donnée = new();


            donnée.Add("Numero Commande \t Numero Client \t Date Commande \t MontantCommande");


            donnée.Add($"{NumeroCommande}\t{NumeroClient}\t{DateCommande}\t{MontantCommande}");

            File.WriteAllLines("./commande.txt", donnée);
        }
        #endregion
    }
}
