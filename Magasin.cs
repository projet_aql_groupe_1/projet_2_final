﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_2_Getion_Magasin
{
    internal class Magasin
    {
        /// <summary>
        /// Attributs
        /// </summary>
        public List<Commande> listCommande = new();
        public List<Client> listClient = new();
        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="listCommande"></param>
        /// <param name="listClient"></param>
        public Magasin(List<Commande> listCommande, List<Client> listClient)
        {
            this.listCommande = listCommande;
            this.listClient = listClient;
        }


        /// <summary>
        /// Permet d'enregistrer toutes les commandes dans un fichier
        /// </summary>
        public void ImprimerToutesCommandes()
        {

            List<string> donnée = new();


            donnée.Add("Nom \tNumero Commande \t Numero Client \t Date Commande \t MontantCommande");
            foreach (Commande cmd in listCommande)
            {
                foreach (Client client in listClient)
                {
                    donnée.Add($"{client.Nom} {client.Prenom}\t{cmd.NumeroCommande}\t{cmd.NumeroClient}\t{cmd.DateCommande}\t{cmd.MontantCommande}");
                }

            }
            File.WriteAllLines("./Magasin.txt", donnée);
        }

    }
}
