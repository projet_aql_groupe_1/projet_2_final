﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_2_Getion_Magasin
{
    #region Attributs et constructeur
    internal class Produit
    {
        /// <summary>
        /// Atributs
        /// </summary>
        int nonrepeter = DateTime.Now.Millisecond;
        private static Random aleatoire = new Random();
        private static int codeAlleatoire = aleatoire.Next(10000, 99999);


        public string Categorie { get; set; }
        public string Nom { get; set; }
        public int CodeBar { get; set; }
        public double Prix { get; set; }

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="categorie"></param>
        /// <param name="nom"></param>
        /// <param name="prix"></param>
        public Produit(string categorie, string nom, int prix)
        {
            Categorie = categorie;
            Nom = nom;
            Prix = prix;


            CodeBar = codeAlleatoire;

        }
        #endregion

        /// <summary>
        /// Afficher Produits
        /// </summary>
        public void AfficherProduit()
        {
            Console.Write("\n" + this.Nom);
            Console.Write("\t\t" + this.CodeBar);
            Console.Write("\t\t" + this.Prix + " $");
            Console.Write("\t\t" + this.Categorie);

        }
    }
}
