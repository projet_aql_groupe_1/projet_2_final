﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_2_Getion_Magasin
{
    internal class Client
    {
        #region Attributs et Constructeur
        /// <summary>
        /// Attributs
        /// </summary>
        private static Random aleatoire = new Random();

        public int NumeroCarteMembre { get; set; }
        public string TypeClient { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Adresse { get; set; }

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="typeClient"></param>
        /// <param name="nom"></param>
        /// <param name="prenom"></param>
        /// <param name="adresse"></param>
        public Client(string typeClient, string nom, string prenom, string adresse)
        {
            TypeClient = typeClient;
            Nom = nom;
            Prenom = prenom;
            Adresse = adresse;

            NumeroCarteMembre = aleatoire.Next(10000, 99999);
        }
        #endregion
    }
}
